AFRAME.registerComponent('upgrade-materials', {
    init() {
        this.texts = []
        this.reloadMaterials({ type: '', level: 0 })
        selectSystem('upgrades').registerListener(this.el, 'materials', 'list')
        this.el.addEventListener('newMaterials', (event) => {
            this.reloadMaterials(event.detail)
        })
    },
    reloadMaterials(upgradeData) {
        let topY = 0.08
        let distance = 0.065
        let counter = 0

        for (let child of this.texts) {
            this.el.removeChild(child)
        }
        this.texts = []
        if (upgradeData.type === '') {
            return
        }

        if (upgradeData.type === 'thrusters') {
            let levels = upgradeData.level.split('.')
            if (parseInt(levels[1]) > 0) {
                this.res =
                    upgradesCatalogue[upgradeData.type][parseInt(levels[0])].efficiencyUpgrades[
                        parseInt(levels[1]) - 1
                    ].resources
            } else {
                this.res = upgradesCatalogue[upgradeData.type][parseInt(levels[0])].resources
            }
        } else {
            this.res = upgradesCatalogue[upgradeData.type][upgradeData.level].resources
        }

        for (let material of this.res) {
            let text1 = createEntity(
                this.el,
                {
                    id: material.id + 'Label',
                    position: '-0.36 ' + (topY - counter * distance) + ' 0.001',
                    scale: '0.25 0.25 0.25',
                    value: translate(material.id),
                },
                'a-text'
            )
            let text2 = createEntity(
                this.el,
                {
                    id: material.id + 'Value',
                    position: '0.36 ' + (topY - counter * distance) + ' 0.001',
                    scale: '0.25 0.25 0.25',
                    align: 'right',
                    value: material.amount.toLocaleString(),
                },
                'a-text'
            )
            this.texts.push(text1)
            this.texts.push(text2)
            counter++
        }
    },
})
