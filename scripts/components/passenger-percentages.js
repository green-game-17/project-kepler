AFRAME.registerComponent('passenger-percentages', {
    init() {
        this.leftXPosition = {
            text: -0.58,
            bar: -0.58,
            icon: -0.58,
        }
        this.distance = {
            text: 0.29,
            bar: 0.29,
            icon: 0.29,
        }
        this.types = ['reproductionRate', 'satisfaction', 'diseaseRisk', 'communitySpirit', 'education']
        this.paths = [
            './assets/icons/icon-reproduction.png',
            './assets/icons/icon-satisfaction.png',
            './assets/icons/icon-disease-risk.png',
            './assets/icons/icon-society.png',
            './assets/icons/icon-education.png',
        ]
        this.texts = []
        this.bars = []
        this.icons = []
        this.maxBarHeight = 0.773
        this.system = selectSystem('passengers')

        for (let type of this.types) {
            let text = createEntity(
                this.el,
                {
                    id: type + 'Percentage',
                    position: this.leftXPosition.text + this.texts.length * this.distance.text + ' 0.088 0.029',
                    scale: '0.4 0.4 0.4',
                    value: '050%',
                    align: 'center',
                },
                'a-text'
            )
            let barY = 0.15 + (this.maxBarHeight * 0.5) / 2
            let bar = createEntity(
                this.el,
                {
                    id: type + 'Bar',
                    position: this.leftXPosition.bar + this.bars.length * this.distance.bar + ' ' + barY + ' 0.01',
                    depth: '0.03',
                    width: '0.2',
                    height: this.maxBarHeight * 0.5 + 0.001,
                    color: '#45e190',
                },
                'a-box'
            )
            let icon = createEntity(
                this.el,
                {
                    id: type + 'Icon',
                    position: this.leftXPosition.icon + this.icons.length * this.distance.icon + ' -0.115 0.029',
                    scale: '0.15 0.15 0.15',
                    src: this.paths[this.icons.length],
                },
                'a-image'
            )
            this.system.registerListener(text, 'text', type)
            this.system.registerListener(bar, 'bar', type)
            text.addEventListener('newPercentage', (event) => {
                text.setAttribute('value', addPercentageZero(event.detail) + '%')
            })
            bar.addEventListener('newPercentage', (event) => {
                let position = bar.getAttribute('position')
                position.y = 0.15 + (this.maxBarHeight * (event.detail / 100)) / 2
                bar.setAttribute('position', position)
                bar.setAttribute('height', this.maxBarHeight * (event.detail / 100) + 0.001)
            })
            this.texts.push(text)
            this.bars.push(bar)
            this.icons.push(icon)
        }
    },
})
