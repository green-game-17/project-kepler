AFRAME.registerComponent('storage-buttons', {
    schema: {
        type: { type: 'string', default: '' },
    },
    init() {
        switch (this.data.type) {
            case 'up':
                this.el.addEventListener('triggered', () => {
                    selectSystem('storage').moveUp()
                })
                break
            case 'down':
                this.el.addEventListener('triggered', () => {
                    selectSystem('storage').moveDown()
                })
                break
            case 'footer':
                this.el.addEventListener('newValue', (event) => {
                    this.el.setAttribute('value', event.detail.toLocaleString() + '/500.000 kg')
                })
                selectSystem('storage').registerListener(this.el, 'footer', 'footer')
                break
            case 'delete':
                this.el.addEventListener('triggered', () => {
                    selectSystem('storage').deleteResource()
                })
                break
            default:
        }
    },
})
