AFRAME.registerComponent('station-marker', {
    schema: {
        animDelay: { type: 'number', default: 0 },
    },

    init: function () {
        let textWhite = '#eaeaea'

        let id = this.el.getAttribute('id')

        this.orbEl = createEntity(this.el, {
            geometry: {
                primitive: 'dodecahedron',
                radius: 0.3,
            },
            material: {
                color: '#3f0909',
            },
            animation: {
                property: 'components.material.material.color',
                type: 'color',
                to: '#ff2605',
                dir: 'alternate',
                dur: 1000,
                delay: this.data.animDelay,
                loop: true,
            },
        })
        let soundCompName = 'sound__start-effect'
        this.el.setAttribute(soundCompName, 'src: #problemSound; volume: 1.2; rolloffFactor: 0.5;')
        this.sound = this.el.components[soundCompName]

        let centerX = this.el.getAttribute('position').x
        this.displayEl = createEntity(this.el, {
            position: `${-8 - centerX} 0 0`,
            rotation: '-17.241 -148.49 79.706',
        })
        createEntity(this.displayEl, {
            geometry: {
                primitive: 'plane',
                width: 6,
                height: 2,
            },
            material: {
                color: '#080808',
                opacity: 0.7,
                transparent: true,
            },
        })
        this.titleEl = createEntity(
            this.displayEl,
            {
                baseline: 'top',
                color: '#cc0a0a',
                animation: {
                    property: 'color',
                    type: 'color',
                    to: '#f22a0c',
                    dir: 'alternate',
                    dur: 1000,
                    delay: this.data.animDelay,
                    loop: true,
                },
                position: '-2.84 0.788 0.001',
                scale: '2.3 2.3 2.3',
            },
            'a-text'
        )
        this.descriptionEl = createEntity(
            this.displayEl,
            {
                baseline: 'top',
                color: textWhite,
                wrapCount: 10,
                position: '-2.808 0.195 0.001',
                scale: '1.5 1.5 1.5',
            },
            'a-text'
        )
        this.resourcesEl = createEntity(
            this.displayEl,
            {
                baseline: 'top',
                color: textWhite,
                position: '-2.806 -0.652 0.001',
                scale: '1.3 1.3 1.3',
            },
            'a-text'
        )

        this.pointerEl = createEntity(this.el, {
            geometry: `primitive: cylinder; openEnded: true; height: ${-9 - centerX}; radius: 0.05`,
            material: {
                side: 'double',
                color: '#080808',
                opacity: 0.5,
                transparent: true,
            },
            shadow: {
                cast: false,
                receive: false,
            },
            rotation: '-20 0 90',
            position: `${(-6.8 - centerX) / 2} 0 0`,
        })

        let trigger = createEntity(this.el, {
            geometry: 'primitive: box; width: 1; height: 1.5; depth: 1.5;',
            position: '-0.5 0 0',
            rotation: '20 0 0',
            visible: false,
        })
        trigger.setAttribute('trigger-target', '')
        trigger.setAttribute('id', id + 'Trigger')
        trigger.addEventListener('triggered', () => {
            if (this.orbEl.getAttribute('visible')) {
                this.displayIsShown = !this.displayIsShown
                this.applyDisplayShown()
            }
        })

        this.el.addEventListener('problemChanged', (evt) => this.applyProblem(evt.detail))

        this.applyVisible(false)

        /* // for testing 
        this.applyProblem({ problem: {
            title:       'E TITLE XXXXXXXXXXXX',
            description: 'E DESCRIPTION |' + 'X'.repeat(25),
            resources:   [ {id: 'softShadows', amount: 9001} ],
        } })
        */
    },

    applyProblem: function ({ problem }) {
        if (!problem) {
            this.applyVisible(false)
            return
        }

        this.titleEl.setAttribute('value', problem.name)
        this.descriptionEl.setAttribute('value', problem.description.replace('|', '\n'))
        let materialStr = problem.resources
            .map((res) => `${res.amount.toLocaleString()} ${translate(res.id)}`)
            .join(', ')
        this.resourcesEl.setAttribute('value', 'needed: ' + materialStr)

        this.displayIsShown = false
        this.applyDisplayShown()

        this.applyVisible(true)

        this.sound.playSound()
    },
    applyVisible: function (vis) {
        this.orbEl.setAttribute('visible', vis)

        this.displayEl.setAttribute('visible', false)
        this.pointerEl.setAttribute('visible', false)
    },
    applyDisplayShown: function () {
        this.displayEl.setAttribute('visible', this.displayIsShown)
        this.pointerEl.setAttribute('visible', this.displayIsShown)
    },
})
