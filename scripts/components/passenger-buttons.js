AFRAME.registerComponent('passenger-buttons', {
    init() {
        this.topLeftPosition = {
            x: -1.161,
            y: -0.422,
        }
        this.distances = {
            x: 0.386,
            y: 0.282,
        }
        this.system = selectSystem('passengers')

        let colors = {
            on: '#228b22',
            off: '#8b2222',
        }

        this.buttonEls = []

        for (let store in storesCatalogue) {
            let x = this.topLeftPosition.x + this.distances.x * (this.buttonEls.length % 7)
            let y = this.topLeftPosition.y + this.distances.y * Math.floor(this.buttonEls.length / 7)
            let button = createEntity(
                this.el,
                {
                    id: store + 'Button',
                    position: x + ' 0.037 ' + y,
                    depth: '0.185',
                    height: '0.033',
                    width: '0.29',
                    color: colors.off,
                },
                'a-box'
            )
            let text = createEntity(
                button,
                {
                    id: store + 'ButtonText',
                    position: '0 0.017 0',
                    rotation: '-90 0 0',
                    scale: '0.17 0.17 0.17',
                    color: '#eeeeee',
                    align: 'center',
                    value: translate(store).length > 20 ? translate(store).replace(' ', '\n') : translate(store),
                },
                'a-text'
            )
            button.setAttribute('trigger-target', '')
            button.addEventListener('triggered', () => {
                if (this.system.activatedStores.includes(store)) {
                    button.setAttribute('color', colors.off)
                    let newPosition = button.getAttribute('position')
                    newPosition.y = 0.037
                    button.setAttribute('position', newPosition)
                    this.system.activatedStores = this.system.activatedStores.filter((key) => key !== store)
                } else {
                    button.setAttribute('color', colors.on)
                    let newPosition = button.getAttribute('position')
                    newPosition.y = 0.017
                    button.setAttribute('position', newPosition)
                    this.system.activatedStores.push(store)
                }
            })
            if (storesCatalogue[store].default) {
                button.setAttribute('trigger-at-start', '')
            }
            this.buttonEls.push(button)
        }
    },
})
