AFRAME.registerComponent('upgrade-modules', {
    schema: {
        type: { default: '' },
    },
    init() {
        this.upgradeData = upgradesCatalogue[this.data.type]

        this.topLeftY = -0.065
        this.distance = 0.13

        let colors = {
            notActive: '#0a0a07',
            active: '#4d4f3b',
            done: '#2f6026',
        }

        for (let i = 0; i < this.upgradeData.length; i++) {
            let button = createEntity(
                this.el,
                {
                    id: this.data.type + i + 'Button',
                    position: '0.137 ' + (this.topLeftY - i * this.distance) + ' 0',
                    depth: '0.04',
                    width: '0.25',
                    height: '0.11',
                    color: colors.notActive,
                },
                'a-box'
            )
            let text = createEntity(
                button,
                {
                    id: this.data.type + i + 'ButtonText',
                    position: '0 0 0.02',
                    rotation: '0 0 0',
                    scale: '0.35 0.35 0.35',
                    color: '#eeeeee',
                    align: 'center',
                    value: i + 1,
                },
                'a-text'
            )
            selectSystem('upgrades').registerListener(button, this.data.type, i)
            button.setAttribute('trigger-target', '')
            button.addEventListener('triggered', () => {
                selectSystem('upgrades').showUpgrade(this.data.type, i)
            })
            button.addEventListener('activate', () => {
                button.setAttribute('color', colors.active)
            })
            button.addEventListener('finish', () => {
                button.setAttribute('color', colors.done)
            })
        }
        let icon = createEntity(
            this.el,
            {
                id: this.data.type + 'UpgradeIcon',
                position: '0.139 ' + (this.topLeftY - this.upgradeData.length * this.distance - 0.02) + ' 0.006',
                scale: '0.15 0.15 0.15',
                src: './assets/icons/icon-' + this.data.type + '.png',
            },
            'a-image'
        )
    },
})
