AFRAME.registerComponent('upgrade-thruster', {
    init() {
        this.topLeftPosition = {
            x: 0.091,
            y: -1.146,
        }
        this.distances = {
            x: 0.203,
            y: 0.257,
        }

        let colors = {
            notActive: '#0a0a07',
            active: '#4d4f3b',
            done: '#2f6026',
        }

        this.upgradeSystem = selectSystem('upgrades')
        this.researchSystem = selectSystem('research')

        this.buttonEls = []

        for (let i = 0; i < upgradesCatalogue.thrusters.length; i++) {
            let y = this.topLeftPosition.y + this.distances.y * i
            let button = createEntity(
                this.el,
                {
                    id: upgradesCatalogue.thrusters[i].id + 'Button',
                    position: this.topLeftPosition.x + ' 0.017 ' + y,
                    depth: '0.215',
                    height: '0.033',
                    width: '0.165',
                    color: colors.notActive,
                },
                'a-box'
            )
            let text = createEntity(
                button,
                {
                    id: upgradesCatalogue.thrusters[i].id + 'ButtonText',
                    position: '0 0.017 0',
                    rotation: '-90 0 0',
                    scale: '0.17 0.17 0.17',
                    color: '#eeeeee',
                    align: 'center',
                    value: upgradesCatalogue.thrusters[i].name.replace(/ /g, '\n'),
                },
                'a-text'
            )
            selectSystem('upgrades').registerListener(button, 'thrusters', i)
            button.setAttribute('trigger-target', '')
            button.addEventListener('triggered', () => {
                if (this.researchSystem.unlocked('thrusters', i + 1)) {
                    if (!this.upgradeSystem.upgradeInProgress) {
                        this.upgradeSystem.showUpgrade('thrusters', i + '.0')
                    }
                }
            })
            button.addEventListener('activate', () => {
                button.setAttribute('color', colors.active)
            })
            button.addEventListener('finish', () => {
                button.setAttribute('color', colors.done)
            })
            for (let j = 0; j < upgradesCatalogue.thrusters[i].efficiencyUpgrades.length; j++) {
                let x = this.topLeftPosition.x + this.distances.x * (j + 1)
                let button = createEntity(
                    this.el,
                    {
                        id: upgradesCatalogue.thrusters[i].efficiencyUpgrades[j].id + 'Button',
                        position: x + ' 0.017 ' + y,
                        depth: '0.215',
                        height: '0.033',
                        width: '0.165',
                        color: colors.notActive,
                    },
                    'a-box'
                )
                let text = createEntity(
                    button,
                    {
                        id: upgradesCatalogue.thrusters[i].efficiencyUpgrades[j].id + 'ButtonText',
                        position: '0 0.017 0',
                        rotation: '-90 0 0',
                        scale: '0.25 0.25 0.25',
                        color: '#eeeeee',
                        align: 'center',
                        value: i + 1 + '.' + (j + 1),
                    },
                    'a-text'
                )
                selectSystem('upgrades').registerListener(button, 'thrusters', i + '.' + (j + 1))
                button.setAttribute('trigger-target', '')
                button.addEventListener('triggered', () => {
                    if (this.researchSystem.unlocked('thrusters', i + 1)) {
                        if (!this.upgradeSystem.upgradeInProgress) {
                            this.upgradeSystem.showUpgrade('thrusters', i + '.' + (j + 1))
                        }
                    }
                })
                button.addEventListener('activate', () => {
                    button.setAttribute('color', colors.active)
                })
                button.addEventListener('finish', () => {
                    button.setAttribute('color', colors.done)
                })
            }
        }
    },
})
