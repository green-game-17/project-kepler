AFRAME.registerComponent('upgrade-text', {
    schema: {
        type: { type: 'string', default: '' },
        attribute: { type: 'string', default: '' },
    },
    init() {
        selectSystem('upgrades').registerListener(this.el, 'text', this.data.type)
        this.el.addEventListener('newText', (event) => {
            if (this.data.attribute === '') {
                this.el.setAttribute(event.detail.attribute, translate(event.detail.value))
            } else if (this.data.type === 'title') {
                this.el.setAttribute(this.data.attribute, translate(event.detail).replace(/ /g, '\n'))
            } else {
                this.el.setAttribute(this.data.attribute, translate(event.detail))
            }
        })
    },
})
