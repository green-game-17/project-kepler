AFRAME.registerComponent('problem-listener', {
    schema: {
        type: 'string',
        default: '',
    },
    init: function () {
        selectSystem('problems').registerListener(this, this.data)
    },
})
