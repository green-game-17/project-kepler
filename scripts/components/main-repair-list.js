AFRAME.registerComponent('main-repair-list', {
    init() {
        this.distances = {
            y: 0.13,
        }
        this.activeButton = null

        let colors = {
            on: '#4d4f3b',
            off: '#0a0a07',
        }

        this.listEls = []

        selectSystem('problems').registerListener(this, 'all')
        this.el.addEventListener('problemChanged', () => this._update())
    },

    _update: function () {
        for (let moduleKey in selectSystem('problems').currentProblems) {
            let module = selectSystem('problems').currentProblems[moduleKey]
            if (module) {
                let y = this.distances.y * this.listEls.length
                let button = createEntity(
                    this.el,
                    {
                        id: 'mainRepair' + module + 'Button',
                        position: '0 ' + y + ' 0',
                        height: '0.12',
                        width: '1.7',
                        color: '#3E3E3E',
                    },
                    'a-panel'
                )
                let title = createEntity(
                    button,
                    {
                        id: 'mainRepair' + module + 'Problem',
                        position: '0 0 0',
                        rotation: '-90 0 0',
                        scale: '0.15 0.15 0.15',
                        value: translate(module),
                    },
                    'a-text'
                )
                let resources = createEntity(
                    button,
                    {
                        id: 'mainRepair' + module + 'Problem',
                        position: '1.6 0 0',
                        rotation: '-90 0 0',
                        scale: '0.15 0.15 0.15',
                        align: 'right',
                        value: translate(module),
                    },
                    'a-text'
                )
                this.listEls.push(button)
            }
        }
    },
})
