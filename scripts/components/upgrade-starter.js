AFRAME.registerComponent('upgrade-starter', {
    init() {
        this.el.addEventListener('triggered', () => {
            if (this.el.getAttribute('color') !== '#0a0a07') {
                selectSystem('upgrades').startUpgrade()
            }
        })
    },
})
