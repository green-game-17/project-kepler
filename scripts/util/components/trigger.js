// First track it:

AFRAME.registerSystem('trigger-track', {
    init: function () {
        this.listeners = []
    },

    registerListener: function (entity) {
        this.listeners.push(entity.el)
    },
    propagateTriggered: function (entity) {
        this.listeners.forEach((l) => l.emit('triggered', { id: entity.id }))
        entity.emit('triggered', {})
    },
})

AFRAME.registerComponent('trigger-target', {
    init: function () {
        let soundCompName = 'sound__trigger-effect'
        this.el.setAttribute(soundCompName, 'src: #buttonSound; volume: 0.7; rolloffFactor: 2;')
        this.sound = this.el.components[soundCompName]

        this.el.addEventListener('mousedown', () => {
            selectSystem('trigger-track').propagateTriggered(this.el)
            this.sound.playSound()
        })
    },
})

// Then trigger stuff with it:

AFRAME.registerComponent('trigger-listener', {
    init: function () {
        selectSystem('trigger-track').registerListener(this)
    },
})

AFRAME.registerComponent('trigger-at-start', {
    init: function () {
        this._done = false
    },
    tick: function () {
        if (!this._done) {
            selectSystem('trigger-track').propagateTriggered(this.el)
            this._done = true
        }
    },
})
