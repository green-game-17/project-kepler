// enable us to be too lazy to type...:
selectEntity = (name) => document.querySelector(`#${name}`)

selectSystem = (name) => document.querySelector('a-scene').systems[name]

createEntity = (parentEl, baseAttrs, type) => {
    let el = document.createElement(type || 'a-entity')
    parentEl.appendChild(el)
    for (let attr in baseAttrs) {
        el.setAttribute(attr, baseAttrs[attr])
    }
    return el
}
