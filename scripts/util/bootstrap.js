// init Sentry
if (window.location.hostname === 'green-game-17.gitlab.io') {
    Sentry.init({ dsn: 'https://bf7cfd24bd9942109710f90807dbe80a@o380774.ingest.sentry.io/5207102' })
}

// set debug mode
const isGreenActivated = AFRAME.utils.getUrlParameter('green')
