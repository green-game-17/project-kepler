pick = (arr) => arr[Math.floor(Math.random() * arr.length)]
pickNum = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min
pickFloat = (min, max) => Math.random() * (max - min + 1) + min

translate = (id) => langCatalogue['english'][id] || id

function addPercentageZero(value) {
    return value < 10 ? '00' + value : value < 100 ? '0' + value : value
}

addTenZero = (value) => (value < 10 ? '0' + value : value)

toMinutes = (seconds) => Math.floor(seconds / 60) + ':' + addTenZero(seconds % 60)

toWeeks = (seconds) => Math.floor(seconds / 52) + ' y ' + addTenZero(seconds % 52) + ' w'

function getIcon(id) {
    return './assets/icons/icon-' + id + '.png'
}
