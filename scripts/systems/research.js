AFRAME.registerSystem('research', {
    init() {
        this.researchLevel = {
            scanner: 0,
            storage: 0,
            thrusters: 0,
            seats: 0,
            food: 0,
            water: 0,
            energy: 0,
            robotics: 0,
        }
        this.currentResearch = ''
        this.researching = ''
        this.researchActive = false
        this.untilFinished = 0
        this._listeners = {}

        let tickInterval = isGreenActivated ? 20 : 1000
        this.tick = AFRAME.utils.throttleTick(this.tick, tickInterval, this)
    },
    tick() {
        if (this.researchActive) {
            let researchData = researchCatalogue[this.researching][this.researchLevel[this.researching]]
            this.untilFinished--
            let percentage = 1 - this.untilFinished / researchData.time
            this._listeners.toggle.progress1.emit('newValue', { attribute: 'width', value: 1.4 * percentage })
            this._listeners.toggle.progress2.emit('newValue', {
                attribute: 'value',
                value: addPercentageZero(Math.round(percentage * 100)) + '%',
            })
            this._listeners.toggle.progress3.emit('newValue', { attribute: 'value', value: this.researching }),
                this._listeners.toggle.progress4.emit('newValue', {
                    attribute: 'value',
                    value: toWeeks(this.untilFinished),
                })
            if (this.untilFinished <= 0) {
                this.finishResearch()
            }
        }
    },
    selectResearch(type) {
        if (type === this.currentResearch) {
            this.currentResearch = ''
            this._listeners.text.level.emit('newValue', 0)
            this._listeners.text.newLevel.emit('newValue', 0)
            this._listeners.text.passengers.emit('newValue', '0/0')
            this._listeners.text.time.emit('newValue', '0:00')
        } else {
            this.currentResearch = type
            let researchData = researchCatalogue[type][this.researchLevel[type]]
            if (researchData) {
                this._listeners.text.level.emit('newValue', this.researchLevel[type])
                this._listeners.text.newLevel.emit('newValue', this.researchLevel[type] + 1)
                this._listeners.text.passengers.emit('newValue', '20/' + researchData.passengers)
                this._listeners.text.time.emit('newValue', toMinutes(researchData.time))
            } else {
                researchData = researchCatalogue[type][this.researchLevel[type] - 1]
                this._listeners.text.level.emit('newValue', this.researchLevel[type])
                this._listeners.text.newLevel.emit('newValue', this.researchLevel[type])
                this._listeners.text.passengers.emit('newValue', '20/' + researchData.passengers)
                this._listeners.text.time.emit('newValue', toMinutes(researchData.time))
            }
        }
    },
    refreshResearch(type) {
        let researchData = researchCatalogue[type][this.researchLevel[type]]
        if (researchData) {
            this._listeners.text.level.emit('newValue', this.researchLevel[type])
            this._listeners.text.newLevel.emit('newValue', this.researchLevel[type] + 1)
            this._listeners.text.passengers.emit('newValue', '20/' + researchData.passengers)
            this._listeners.text.time.emit('newValue', toMinutes(researchData.time))
        } else {
            researchData = researchCatalogue[type][this.researchLevel[type] - 1]
            this._listeners.text.level.emit('newValue', this.researchLevel[type])
            this._listeners.text.newLevel.emit('newValue', this.researchLevel[type])
            this._listeners.text.passengers.emit('newValue', '20/' + researchData.passengers)
            this._listeners.text.time.emit('newValue', toMinutes(researchData.time))
        }
    },
    startResearch() {
        if (!this.researchActive && this.currentResearch !== '') {
            this._listeners.toggle.button.emit('newValue', false)
            this._listeners.toggle.progress1.emit('newValue', { attribute: 'visible', value: true })
            this._listeners.toggle.progress2.emit('newValue', { attribute: 'visible', value: true })
            this._listeners.toggle.progress3.emit('newValue', { attribute: 'visible', value: true })
            this._listeners.toggle.progress4.emit('newValue', { attribute: 'visible', value: true })
            this.untilFinished = researchCatalogue[this.currentResearch][this.researchLevel[this.currentResearch]].time
            this.researching = this.currentResearch
            this.researchActive = true
        }
    },
    finishResearch() {
        this._listeners.toggle.button.emit('newValue', true)
        this._listeners.toggle.progress1.emit('newValue', { attribute: 'visible', value: false })
        this._listeners.toggle.progress2.emit('newValue', { attribute: 'visible', value: false })
        this._listeners.toggle.progress3.emit('newValue', { attribute: 'visible', value: false })
        this._listeners.toggle.progress4.emit('newValue', { attribute: 'visible', value: false })
        this.researchActive = false
        selectSystem('upgrades').activateButton(this.researching, this.researchLevel[this.researching])
        this.researchLevel[this.researching] += 1
        if (this.currentResearch === this.researching) {
            this.refreshResearch(this.researching)
        }
        this.researching = ''
    },
    unlocked(type, level) {
        return this.researchLevel[type] >= level
    },
    registerListener(entity, type, id) {
        if (!this._listeners[type]) {
            this._listeners[type] = {}
        }
        this._listeners[type][id] = entity
    },
})
