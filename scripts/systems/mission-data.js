AFRAME.registerSystem('mission-data', {
    init: function () {
        this._listeners = {
            passengers: [],
            arrivalTime: [],
            currentWeek: [],
        }
        this._active = true

        this._passengerCount = 200
        this._distance = 2_840_098_740_000_000 // 300 ly
        this._playedTime = 0

        // Hello at whoever does balancing ;)
        this._passengerDampeningFactor = 500
        this._limitYear = 5000

        let interval = isGreenActivated ? 10 : 1000
        this.tick = AFRAME.utils.throttleTick(this.tick, interval, this)
    },
    tick: function () {
        if (!this._active) {
            return
        }

        let { reproductionRate, diseaseRisk } = selectSystem('passengers').currentValues
        this._passengerCount += (this._passengerCount / this._passengerDampeningFactor) * reproductionRate
        this._passengerCount -=
            (this._passengerCount / this._passengerDampeningFactor) *
            (0.0077 /* base mortality rate of 0.77% (world average) */ +
                diseaseRisk * 0.95) /* max mortality of 95% (i.e. black death) */
        this._emit('passengers', Math.floor(this._passengerCount))

        this._playedTime += 1
        let elapsedYears = Math.floor(this._playedTime / 52)
        let currentYear = 2025 + elapsedYears
        let currentWeek = addTenZero(this._playedTime % 52)
        let currentTime = `year ${currentYear}\nweek ${currentWeek} `
        this._emit('currentWeek', currentTime)

        let thrusterLevel = selectSystem('upgrades').upgradeLevel.thrusters.main
        let thrusterSpeed = [2, 10, 60, 420, 3_360, 28_080][thrusterLevel] * 1_000_000
        let travelled = thrusterSpeed * 24 * 7
        this._distance -= travelled
        let remainingTime = this._toWeeks2(this._distance / travelled)
        this._emit('arrivalTime', remainingTime)

        let seatLevel = selectSystem('upgrades').upgradeLevel.seats
        let seatCount = [250, ...upgradesCatalogue['seats'].map((x) => x.result)][seatLevel]

        if (currentYear > this._limitYear) {
            this._end('death', { reason: 'Journey took too long, you got outpaced' })
        }
        if (this._passengerCount < 80) {
            this._end('death', { reason: 'Population got too small to safely reproduce' })
        }
        if (this._passengerCount > seatCount * 1.5) {
            this._end('death', { reason: 'Population got too big for the available seat count' })
        }

        if (this._distance < 1) {
            this._end('win', { time: elapsedYears })
        }
    },

    registerListener: function (entity, type) {
        this._listeners[type].push(entity.el)
    },

    _emit: function (type, data) {
        this._listeners[type].forEach((l) => l.emit('newValue', data))
    },
    _toWeeks2: function (seconds) {
        if (seconds > 52) {
            return Math.floor(seconds / 52).toLocaleString() + ' y '
        } else {
            return addTenZero(Math.floor(seconds % 52)) + ' w'
        }
    },
    _end: function (type, data) {
        this._active = false
        selectSystem('game-state').set(type, data)
    },
})

AFRAME.registerComponent('mission-data-listener', {
    schema: { type: 'string' },
    init: function () {
        selectSystem('mission-data').registerListener(this, this.data)
    },
})
