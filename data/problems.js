const problemCatalogue = {
    thrusters: [
        {
            name: 'Thrusters',
            description: 'The thrusters are down.',
            resources: [
                { id: ['molybdenum', 'tantalum', 'cobalt', 'zirconium', 'tungsten'], minAmount: 1000, maxAmount: 1500 },
            ],
        },
        {
            name: 'Thrusters',
            description: 'The thrusters are overheating.',
            resources: [
                { id: 'potassium', minAmount: 1250, maxAmount: 1750 },
                { id: 'sodium', minAmount: 1000, maxAmount: 1680 },
            ],
        },
        {
            name: 'Thrusters',
            description: 'The thruster control is down.',
            resources: [
                { id: 'copper', minAmount: 320, maxAmount: 590 },
                { id: 'gold', minAmount: 120, maxAmount: 260 },
            ],
        },
    ],
    storage: [
        {
            name: 'Storage',
            description: 'The storage room has a hull breach.',
            resources: [{ id: 'aluminium', minAmount: 1350, maxAmount: 2200 }],
        },
        {
            name: 'Storage',
            description: 'A storage rack broke.',
            resources: [{ id: 'iron', minAmount: 1000, maxAmount: 1670 }],
        },
        {
            name: 'Storage',
            description: 'The storage sorting system has failed.',
            resources: [{ id: 'copper', minAmount: 120, maxAmount: 135 }],
        },
    ],
    habitat: [
        {
            name: 'Habitat',
            description: 'There is a hole in the habitat.',
            resources: [
                { id: 'aluminium', minAmount: 3000, maxAmount: 4500 },
                { id: 'iron', minAmount: 2500, maxAmount: 3400 },
            ],
        },
        {
            name: 'Habitat',
            description: 'The habitat has stopped spinning.',
            resources: [
                { id: 'copper', minAmount: 450, maxAmount: 670 },
                { id: 'gold', minAmount: 80, maxAmount: 200 },
            ],
        },
    ],
    shield: [
        {
            name: 'Particle Shield',
            description: 'There is a hole in the particle shield.',
            resources: [
                { id: 'iron', minAmount: 350, maxAmount: 740 },
                { id: 'nickel', minAmount: 120, maxAmount: 265 },
            ],
        },
    ],
    spine: [
        {
            name: 'Station Spine',
            description: 'The fuel tank has a leak.',
            resources: [{ id: 'aluminium', minAmount: 800, maxAmount: 1560 }],
        },
        {
            name: 'Spine',
            description: 'Some cables got disconnected.',
            resources: [{ id: 'copper', minAmount: 1200, maxAmount: 3600 }],
        },
        {
            name: 'Spine',
            description: 'The spine is cracked.',
            resources: [
                { id: 'aluminium', minAmount: 200, maxAmount: 560 },
                { id: 'carbon', minAmount: 1200, maxAmount: 1800 },
            ],
        },
    ],
    hangar: [
        {
            name: 'Hangar',
            description: 'The hangar has a hull breach.',
            resources: [{ id: 'aluminium', minAmount: 450, maxAmount: 980 }],
        },
        {
            name: 'Hangar',
            description: 'A robot is running amok in the hangar.',
            resources: [{ id: 'robots', minAmount: 5, maxAmount: 10 }],
        },
        {
            name: 'Hangar',
            description: 'A fire is active in the hangar.',
            resources: [{ id: 'ice', minAmount: 2500, maxAmount: 4000 }],
        },
    ],
}
