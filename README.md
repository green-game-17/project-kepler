# ProjectKepler

_**"A way too complicated space ship management game"**_

The time is future.

And in the future - earth didn't manage to keep alive. So humanity set out to colonize other systems, launching an advanced space ship towards the exoplanet Kepler 1649c.

After five years of travel, the ship's captain decided to hand over control to you - the most advanced AI available on the ship. So your tasks becomes to manage all the ships systems, to order new research and upgrades and to fix the ever appearing problems of the ship. All with the goal to reach the target planet before your population turns sour or you get outpaced by another ship.

### develop locally

-   set-up everything
    1. make sure that you've [yarn v1](https://classic.yarnpkg.com/en/docs/install#windows-stable) installed
    1. install dependencies by calling `yarn` in a terminal
-   to start a local server with live reloading: `yarn serve`
-   to start the aframe-watcher so sync changes in the inspector back to HTML: `yarn aframe-watcher`

### testing

-   to speed up various timing, append `?green=yes` to the URL
-   to jump right into the inspector, append `?inspect=yes` to the URL
