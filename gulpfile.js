const { src, dest, watch, series } = require('gulp')
const concat = require('gulp-concat')
const replace = require('gulp-replace')
const browserSync = require('browser-sync').create()

const jsSrcGlobs = [
    // define the include order by changing the file-path-globs here
    'node_modules/@sentry/browser/build/bundle.min.js',
    'node_modules/aframe/dist/aframe.min.js',
    'scripts/util/bootstrap.js',
    'scripts/util/*.js',
    'scripts/util/components/*.js',
    'data/**/*.js',
    'scripts/systems/*.js',
    'scripts/components/**/*.js',
]
const watchGlobs = [
    // define extra file-path-globs to just watch and trigger browser reload on changes
    'index.html',
    'assets/**/*.*',
]

function packageJs() {
    return src(jsSrcGlobs).pipe(concat('dist.js')).pipe(dest('scripts')).pipe(browserSync.stream())
}

function copyAframeInspector() {
    return src('node_modules/aframe-inspector/dist/aframe-inspector.min.js').pipe(dest('scripts'))
}

function startBrowserSync() {
    browserSync.init({
        server: {
            baseDir: './',
        },
    })

    watch(jsSrcGlobs, packageJs)
    watch(watchGlobs, function reload(cb) {
        browserSync.reload()
        cb()
    })
}

function tagVersion() {
    const version = require('./package.json').version
    const marker = `<div id="versionMarker">v${version}</div>`
    return src('index.html').pipe(replace('<div id="versionMarker">DEV</div>', marker)).pipe(dest('.'))
}

exports.serve = series(packageJs, copyAframeInspector, startBrowserSync)
exports.package = series(packageJs, tagVersion)
